using Graphite.Model;
using Graphite.Types;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Core.Events;
using OpenTelemetry.Metrics;
using OpenTelemetry.Resources;
using OpenTelemetry.Trace;
using Serilog;
using Serilog.Events;
using Serilog.Templates.Themes;
using SerilogTracing;
using SerilogTracing.Expressions;

Log.Logger = new LoggerConfiguration()
    .MinimumLevel.Override("Microsoft.AspNetCore.Hosting", LogEventLevel.Warning)
    .MinimumLevel.Override("Microsoft.AspNetCore.Routing", LogEventLevel.Warning)
    .Enrich.WithProperty("Application", "ExampleService")
    .WriteTo.Console(Formatters.CreateConsoleTextFormatter(theme: TemplateTheme.Code))
    .WriteTo.Seq(
        Environment.GetEnvironmentVariable("SEQ_URL") ?? "http://localhost:5341"
    ) 
    .CreateLogger();

using var listener = new ActivityListenerConfiguration()
    .Instrument.AspNetCoreRequests()
    .TraceToSharedLogger();

try
{
    var builder = WebApplication.CreateBuilder(args);

    builder.Services.AddSerilog();
    
    builder.Services
        .AddSingleton(sp =>
        {
            const string connectionString = "mongodb://root:example@localhost:27017/?authSource=admin";
            var mongoConnectionUrl = new MongoUrl(connectionString);
            var mongoClientSettings = MongoClientSettings.FromUrl(mongoConnectionUrl);
            mongoClientSettings.ClusterConfigurator = cb =>
            {
                // This will print the executed command to the console
                cb.Subscribe<CommandStartedEvent>(e =>
                {
                    Log.Logger.Information("{CommandName} - {Command}", e.CommandName, e.Command.ToJson());
                });
            };
            var client = new MongoClient(mongoClientSettings);
            var database = client.GetDatabase("test");
            return database.GetCollection<Person>("person");
        });

    builder.Services
        .AddGraphQLServer()
        .AddQueryType<Query>()
        .AddMutationType<Mutation>()
        .AddGlobalObjectIdentification()
        .AddMongoDbFiltering()
        .AddMongoDbSorting()
        .AddMongoDbProjections()
        .AddMongoDbPagingProviders()
        .AddInstrumentation();
    
    var tracingOtlpEndpoint = "http://localhost:4317/"; //jaeger
    
    builder.Services.AddOpenTelemetry()
        .ConfigureResource(b => b.AddService("Graphite"))
        .WithTracing(b => 
            b.AddAspNetCoreInstrumentation()
                .AddHttpClientInstrumentation()
                .AddAspNetCoreInstrumentation()
                .AddHotChocolateInstrumentation()
                .AddOtlpExporter(opts => { opts.Endpoint = new Uri(tracingOtlpEndpoint); }))
        .WithMetrics(b => 
            b.AddAspNetCoreInstrumentation()
                .AddHttpClientInstrumentation()
                .AddAspNetCoreInstrumentation()
                .AddRuntimeInstrumentation()
                .AddProcessInstrumentation()
                // https://github.com/open-telemetry/opentelemetry-dotnet/issues/5502
                .AddPrometheusExporter(o => o.DisableTotalNameSuffixForCounters = true)
                .AddMeter("Microsoft.AspNetCore.Hosting", "Microsoft.AspNetCore.Server.Kestrel")
                .AddView("http.server.request.duration",
                    new ExplicitBucketHistogramConfiguration
                    {
                        Boundaries =
                        [
                            0, 0.005, 0.01, 0.025, 0.05,
                            0.075, 0.1, 0.25, 0.5, 0.75, 1, 2.5, 5, 7.5, 10
                        ]
                    }));


    var app = builder.Build();

    app.UseOpenTelemetryPrometheusScrapingEndpoint();
    
    app.MapGraphQL();

    app.Run();

}
catch (Exception ex)
{
    Log.Fatal(ex, "Unhandled exception");
    return 1;
}
finally
{
    await Log.CloseAndFlushAsync();
}

return 0;