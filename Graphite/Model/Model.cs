﻿using MongoDB.Driver;

namespace Graphite.Model;

[Node(
    IdField = nameof(Id),
    NodeResolverType = typeof(PersonNodeResolver),
    NodeResolver = nameof(PersonNodeResolver.ResolveAsync))]
public class Person
{
    public Guid Id { get; init; }

    public string Name { get; init; }

    public IReadOnlyList<Address> Addresses { get; init; }

    public Address MainAddress { get; init; }
}

public class PersonNodeResolver
{
    public Task<Person> ResolveAsync(
        [Service] IMongoCollection<Person> collection,
        Guid id)
    {
        return collection.Find(x => x.Id == id).FirstOrDefaultAsync();
    }
}

public class Address
{
    public string Street { get; init; }

    public string City { get; init; }
}

public record CreatePersonInput(
    string Name,
    IReadOnlyList<Address> Addresses,
    Address MainAddress);
    
public record CreatePersonPayload(Person Person);